package controllers;

import com.google.inject.Inject;
import controllers.security.Authenticator;
import models.api.Mail;
import models.constants.AccountType;
import models.entities.Account;
import models.forms.account.AccountForm;
import models.forms.account.LoginForm;
import models.forms.account.VerificationForm;
import models.services.account.AccountModelService;
import models.services.account.AccountService;
import models.services.user.UserModelService;
import models.services.user.UserService;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.account.accountTypeSelect;
import views.html.account.emailCheck;
import views.html.account.login;
import views.html.account.register;
import views.html.index;

import java.util.Optional;

import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/05/08.
 * 共通のアカウント認証やマイページへの遷移を記述
 */
public class AccountController extends Controller {

    @Inject
    MailerClient mailerClient;

    @Inject
    FormFactory formFactory;

    @Inject
    Authenticator authenticator;

    @Inject
    AccountService accountService;

    @Inject
    AccountModelService accountModelService;

    @Inject
    UserService userService;

    @Inject
    UserModelService userModelService;

    /**
     * @return アカウント新規登録画面
     */
    public Result register() {
        Form<AccountForm> accountForm = formFactory.form(AccountForm.class);
        return ok(register.render(accountForm, "ログインに使用するメールアドレスとパスワードを入力してください"));
    }

    /**
     * POST 送信時の失敗処理再読み込み
     *
     * @pram ユーザの入力情報、エラーメッセージ
     */
    public Result registerFailed(Form<AccountForm> accountForm, String message) {
        return badRequest(register.render(accountForm, message));
    }

    /**
     * 登録画面のボタンPOST送信処理
     */
    public Result signUp() {
        Form<AccountForm> entry = formFactory.form(AccountForm.class).bindFromRequest();

        //フォームに誤りがあれば戻る
        if (entry.hasErrors()) {
            for (ValidationError error : entry.globalErrors()) {
                Logger.info("validation error:" + error.message());
            }
            return registerFailed(entry, "入力に誤りがあります");
        }

        //入力したメールアドレスで検索して同じものがあればAccountを取得する
        Optional<Account> accountOps = accountModelService.findByEmailAddress(entry.get().emailAddress);

        //Accountが登録されていれば、そのアカウントを返す
        if (accountOps.isPresent()) {
            return registerFailed(entry, "入力されたメールアドレスは既に使用されています");
        }

        //入力情報をもとにアカウント作成
        Boolean result = accountService.accountRegister(entry.get());

        //登録の成功確認
        if (result == SUCCESS) {
            //登録したアカウントを取得してセッションに保存
            accountOps = accountModelService.findByEmailAddress(entry.get().emailAddress);
            if (accountOps.isPresent()) {
                authenticator.sessionRegister(accountOps.get());
            } else {
                return loginFailed(formFactory.form(LoginForm.class), "情報の取得に失敗しました ログインをしてください");
            }
        } else if (result == FAILURE) {
            return registerFailed(entry, "登録に失敗しました");
        }

        return emailCheck();
    }

    /**
     * 登録しようとしたメールアドレスにメールを送り、
     * 6桁の数字によるメールの確認を行う
     *
     * @return
     */
    public Result emailCheck() {

        //ログイン中のアカウントをセッションから取得
        //セッションにランダムな(100000~999999)の値を生成して格納
        Account account = authenticator.getSessionAccount();
        String randomToken = authenticator.setRandomToken();

        // アカウントアドレスに格納したトークンをメールで送信
        Mail mail = new Mail();
        Boolean result = mail.emailCheck(mailerClient, account, randomToken);

        if (result == SUCCESS) {
            //リロードでメールを再送しないためリダイレクト
            //return redirect("/account/verify-email");
            return redirect(routes.AccountController.tokenCheck());
        }
        return loginFailed(formFactory.form(LoginForm.class), "登録確認メールを送信できませんでした 再度ログインしてください");
    }

    /**
     * @return 二段階認証用トークン入力フォーム
     */
    public Result tokenCheck() {
        Form<VerificationForm> verificationForm = formFactory.form(VerificationForm.class);
        return ok(emailCheck.render(verificationForm, "登録確認メールを送信しました"));
    }

    /**
     * POST 送信時の失敗処理再読み込み
     *
     * @pram ユーザの入力情報、エラーメッセージ
     */
    public Result tokenCheckFailed(Form<VerificationForm> verificationForm, String message) {
        return badRequest(emailCheck.render(verificationForm, message));
    }

    /**
     * 二段階認証のトークン確認画面
     *
     * @return
     */
    public Result verifyToken() {

        Form<VerificationForm> entry = formFactory.form(VerificationForm.class).bindFromRequest();

        //フォームに誤りがあれば戻る
        if (entry.hasErrors()) {
            for (ValidationError error : entry.globalErrors()) {
                Logger.info("validation error:" + error.message());
            }
            return tokenCheckFailed(entry, "入力に誤りがあります");
        }

        /*認証処理開始****************************************************/

        //セッションの2段階認証用ランダムトークンを取得する
        String emailToken = entry.get().token;
        String verifyToken = authenticator.getRandomToken();

        //トークン認証成功処理
        if (emailToken.equals(verifyToken)) {

            //ログイン中のアカウントを取得してメール認証をtrueに
            Account loginAccount = authenticator.getSessionAccount();
            loginAccount.confirmation = SUCCESS;

            //データベースアップデートして最新情報に更新
            Boolean result = accountService.update(loginAccount);

            //データベースの更新が成功したらセッション情報も更新する
            if (result == SUCCESS) {
                loginAccount = authenticator.getSessionAccount();
                authenticator.sessionRegister(loginAccount);
            } else {
                return loginFailed(formFactory.form(LoginForm.class), "認証情報更新に失敗しました 再度ログインして認証してください");
            }
        } else {
            return tokenCheckFailed(entry, "確認用トークンが違います");
        }

        //アカウントの状態によって振り分け
        return gateway();
    }

    /**
     * 企業登録か求職者登録か選択する。
     * リンクでユーザ登録画面or企業登録画面
     *
     * @return 選択画面。
     */
    public Result accountTypeSelect() {
        return ok(accountTypeSelect.render());
    }


    /**
     * @return ログイン情報入力画面
     */
    public Result login() {
        Form<LoginForm> loginForm = formFactory.form(LoginForm.class);
        return ok(login.render(loginForm, "ログイン情報を入力してください"));
    }

    /**
     * POST 送信時の失敗処理再読み込み
     *
     * @pram ユーザの入力情報、エラーメッセージ
     */
    public Result loginFailed(Form<LoginForm> loginForm, String message) {
        return badRequest(login.render(loginForm, message));
    }

    /**
     * ログイン情報入力後、Post送信時のアカウント判定
     *
     * @return 認証結果で分岐。OKならgatewayでマイページ分岐
     */
    public Result signIn() {
        //前回ログインしていたユーザのセッションをクリア。
        session().clear();

        Form<LoginForm> entry = formFactory.form(LoginForm.class).bindFromRequest();

        //フォームに誤りがあれば戻る
        if (entry.hasErrors()) {
            for (ValidationError error : entry.globalErrors()) {
                Logger.info("validation error:" + error.message());
            }
            return loginFailed(entry, "入力に誤りがあります");
        }

        /*認証処理開始****************************************************/

        //入力したメールアドレスで検索して同じものがあればAccountを取得する
        Optional<Account> accountOps = accountModelService.findByEmailAddress(entry.get().emailAddress);

        //メールアドレスが登録されていれば
        if (accountOps.isPresent()) {

            //authenticateメソッドの結果でレンダー先を変更
            Boolean result = accountService.authenticate(entry.get());

            if (result == SUCCESS) {
                authenticator.sessionRegister(accountOps.get());
                return gateway();
            } else if (result == FAILURE) {
                return loginFailed(entry, "パスワードが間違っています");
            } else {
                return loginFailed(entry, "何らかの理由で認証に失敗しました　もう一度ログインをお願いします");
            }
        }
        //アカウントが削除されたか、メールアドレスが変更されたかは判別できない
        return loginFailed(entry, "メールアドレスが登録されていません");
    }

    /**
     * この先、ログイン済みアカウントのみ遷移可能
     * ログイン中のAccountTypeによって表示マイページを振り分け(redirectでリバースルーティング)
     * PROVISIONAL = 仮登録   -> 企業登録がユーザ登録か選択
     * USER = 求職者   -> 求職者マイページへ
     * AGENT = 企業    -> 企業マイページへ
     * ADMIN = 管理者  -> adminページへ
     */
    @Security.Authenticated(Authenticator.class)
    public Result gateway() {

        //ログイン中アカウント取得
        Account account = authenticator.getSessionAccount();
        AccountType accountType = account.accountType;

        //メールチェック
        //メール未確認なら二段階認証画面へ
        if (account.confirmation == FAILURE) {
            //未確認ならメールチェックへ
            return emailCheck();
        }

        switch (accountType) {
            case PROVISIONAL:
                //新規ユーザー登録ページへ
                //return redirect("/user/accountInfoRegister");
                return redirect(routes.AccountController.accountTypeSelect());

            case USER:
                //求職者ページへ
                //return redirect("/user/my-page");
                return redirect(routes.UserController.myPage());

            case AGENT:
                //企業マイページへ
                return redirect(routes.AgentController.myPage());

            case ADMIN:
                //管理者ページへ
                break;
        }

        //不正なTypeの場合はログイン画面へ戻す
        return loginFailed(formFactory.form(LoginForm.class), "正しいアカウント情報でログインしてください");
    }

    public Result logout() {
        session().clear();
        return ok(index.render("ログアウトしました"));
    }
}
