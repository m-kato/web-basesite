package controllers;

import com.google.inject.Inject;
import controllers.security.Authenticator;
import models.entities.Account;
import models.entities.User;
import models.forms.account.AccountInfoForm;
import models.services.account.AccountModelService;
import models.services.account.AccountService;
import models.services.user.UserService;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.user.*;

import static models.constants.AccountType.USER;
import static models.constants.Util.SUCCESS;

/**
 * Created by m-kato on 2017/04/27.
 * ユーザが利用できる画面の画面遷移を記述する。
 * ここで行うのは、Formの生成とデータの受け渡し、
 * 受け取ったデータの表示処理(render)のみ行う。
 * <p>
 * ビジネスロジックはUserServiceクラスで記述し、
 * DB処理を行う場合は、UserServiceクラスを通してUserModelServiceで行う
 */
@Security.Authenticated(Authenticator.class)
public class UserController extends Controller {

    @Inject
    Authenticator authenticator;

    @Inject
    FormFactory formFactory;

    @Inject
    AccountService accountService;

    @Inject
    AccountModelService accountModelService;

    @Inject
    UserService userService;

    /**
     * Userの登録処理画面
     */
    public Result register(){
        Form<AccountInfoForm> AccountInfoForm = formFactory.form(AccountInfoForm.class).bindFromRequest();
        return ok(userRegister.render(AccountInfoForm, "アカウント情報をご入力ください"));
    }

    //POST 送信時の失敗処理再読み込み
    public Result register(Form<AccountInfoForm> AccountInfoForm, String message) {
        return badRequest(userRegister.render(AccountInfoForm, message));
    }

    /**
     * 登録画面のボタンPOST送信処理
     */
    public Result signUp() {
        Form<AccountInfoForm> entry = formFactory.form(AccountInfoForm.class).bindFromRequest();

        //Validation Check
        if (entry.hasErrors()) {
            for (ValidationError error : entry.globalErrors()) {
                Logger.info("validation error:" + error.message());
            }
            return register(entry, "入力に誤りがあります");
        }

        /*個人情報入力開始****************************************************/

        //入力情報をもとにアカウント作成
         Boolean result = userService.userRegister(entry.get());

        //登録に成功したらアカウントのタイプを変更してマイページへ
        if (result == SUCCESS) {
            return redirect(routes.AccountController.gateway());
        }
        return register(entry, "登録に失敗しました");
    }

    /**
     * マイページ表示処理
     */
    public Result myPage(){
        Account account = authenticator.getSessionAccount();
        return ok(userMyPage.render(account,"ようこそ、" + account.getFullName() + "さま"));
    }
}


