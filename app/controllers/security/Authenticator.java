package controllers.security;


import com.google.inject.Inject;
import controllers.AccountController;
import models.entities.Account;
import models.forms.account.LoginForm;
import models.services.account.AccountModelService;
import play.cache.DefaultCacheApi;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import static play.mvc.Controller.*;

/**
 * Created by m-kato on 2017/05/11.
 * コントローラのResultに@Security.Authenticated(Authenticator.class)アノテーションをつけて
 * ログイン後のみ閲覧できるページを制限するための認証処理クラス
 */
public class Authenticator extends Security.Authenticator {

    @Inject
    DefaultCacheApi defaultCacheApi;

    @Inject
    FormFactory formFactory;

    @Inject
    AccountModelService accountModelService;

    @Inject
    AccountController accountController;

    private static final String sessionId = "AccountID";
    private static final String userName = "UserName";

    /**
     * 未認証状態でアクセスされたときのアクション
     */
    @Override
    public Result onUnauthorized(Http.Context arg0) {
        // ログインページへジャンプします
        Form<LoginForm> loginForm = formFactory.form(LoginForm.class);
        return accountController.loginFailed(loginForm,"長時間操作がなかった為、ログアウトしました");
    }

    @Override
    public String getUsername(Http.Context context) {

        // ログインクッキーなし
        String userToken = context.session().get(sessionId);
        if (userToken == null){
            return null;
        }

        //キャッシュからトークンを使いaccountの情報を取得
        Account account = defaultCacheApi.get(userToken);

        if (account == null){
            // すでにキャッシュにないのでクッキーも破棄
            context.session().remove(sessionId);
            context.session().remove(userName);
            return null;
        }

        //セッションの時間とトークンを更新
        sessionRegister(account);

        //ログイン中のアカウントIDのメールアドレスを返す
        return account.toString();
    }

    /**
     * Account情報をランダムトークンでキャッシュに格納
     * @param account
     */
    public void sessionRegister(Account account) {

        // ランダムトークン作成
        final String userToken = UUID.randomUUID().toString();

        //セッションの保持時間(秒) 30分
        final Integer sessionLimit = 60 * 30;

        session(sessionId, userToken);
        defaultCacheApi.set(userToken, account, sessionLimit);

        //ログインユーザの名前をセッションに保存
        if(account.hasName()) {
            String loginUserName = account.getFullName();
            session(userName, loginUserName);
        }
    }

    /**
     * sessionTokenからログイン中のアカウントの最新情報を取得
     * @return Account
     */
    public Account getSessionAccount(){
        String userToken = session().get(sessionId);
        Account account = defaultCacheApi.get(userToken);

        //IDは不変のためidで最新のAccount情報を検索
        Long accountId = account.id;
        Optional<Account> accountOps = accountModelService.findById(accountId);

        //取れればセッションを最新情報に更新の上を最新情報を返す
        accountOps.ifPresent(this::sessionRegister);

        return accountOps.orElse(null);
    }

    public void sessionClear(){
        session().clear();
    }

    /**
     * セッションにランダムな(100000~999999)の値を生成して格納
     * そのトークンの値を返す。
     * @return 一時的なトークン
     */
    public String setRandomToken() {
        //セッションにランダムな(100000~999999)の値を生成して格納
        int randomToken = new Random().nextInt(899999);
        randomToken += 100000;
        session("Token",String.format("%06d", randomToken));
        return session("Token");
    }

    public String getRandomToken() {
        return session("Token");
    }

}
