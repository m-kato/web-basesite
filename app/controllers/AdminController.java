package controllers;

import com.google.inject.Inject;
import models.services.admin.AdminService;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.admin.dbInfo;

/**
 * Created by m-kato on 2017/04/28.
 * 管理者の
 */
public class AdminController extends Controller{

    @Inject
    FormFactory formFactory;

    @Inject
    AdminService adminService;

    public Result showTableData(){
        return ok(dbInfo.render(adminService.getAccountList(),adminService.getUserList(),adminService.getCompanyList()));
    }
}
