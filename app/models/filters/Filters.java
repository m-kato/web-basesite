package models.filters;

import javax.inject.*;
import play.*;
import play.filters.gzip.GzipFilter;
import play.mvc.EssentialFilter;
import play.http.HttpFilters;

/**
 * このクラスは、すべてのリクエストで実行されるフィルタをコンフィグレーションします。
 * このクラスは、Playによってフィルタのリストを取得するためにクエリされます。
 *
 * Playは自動的に呼び出されたクラスのフィルタを使用します
 */
@Singleton
public class Filters implements HttpFilters {

    private final Environment env;
    private final EssentialFilter exampleFilter;

    /**
     * @param env Basic environment settings for the current application.
     * @param exampleFilter A demonstration filter that adds a header to
     */
    @Inject
    public Filters(Environment env, ExampleFilter exampleFilter) {
        this.env = env;
        this.exampleFilter = exampleFilter;
    }

    @Override
    public EssentialFilter[] filters() {
      // Use the example filter if we're running development mode. If
      // we're running in production or test mode then don't use any
      // filters at all.
      if (env.mode().equals(Mode.DEV)) {
          return new EssentialFilter[] { exampleFilter };
      } else {
         return new EssentialFilter[] {};
      }
    }

}
