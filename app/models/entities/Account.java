package models.entities;

import models.constants.AccountType;
import models.constants.Gender;
import play.data.validation.Constraints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

import static models.constants.AccountType.PROVISIONAL;

/**
 * Created by m-kato on 2017/04/26.
 * アカウント情報を保持するエンティティ
 * すべての利用者はアカウント情報を持っているため、
 * これとリレーションを持った詳細情報テーブルに情報を格納すること。
 */
@Entity
@Table(name = "account")
public class Account extends BaseEntity {

    @OneToOne(mappedBy = "account")
    public Agent agent;

    @OneToOne(mappedBy = "account")
    public User user;

    //初期化
    public Account(){
        this.emailAddress = "";
        this.password = "";
        this.confirmation = false;
        this.accountType = AccountType.PROVISIONAL;
        this.familyName = "";
        this.firstName = "";
        this.familyNameKana = "";
        this.firstNameKana = "";
        this.gender = Gender.MALE;
        this.birthday = LocalDate.of(1990,1,1);
        this.phoneNumber = "";
    }

    /**
     * ログイン用メールアドレス
     */
    @Constraints.Email
    @NotNull
    @Column(name = "email_address", unique = true)
    public String emailAddress;

    /**
     * ログイン用パスワード
     */
    @NotNull
    @Column(name = "password")
    public String password;

    /**
     * ユーザのタイプ
     * false = 未確認
     * true = 確認済み
     */
    @NotNull
    @Column(name = "confirmation")
    public Boolean confirmation;

    /**
     * 仮登録者(PROVISIONAL)
     * 求職者(User)
     * 採用担当(Agent)
     * 管理者(Admin)
     *
     * @see AccountType
     */
    @NotNull
    @Column(name = "account_type")
    public AccountType accountType;

    @NotNull
    @Column(name = "family_name")
    public String familyName;

    @NotNull
    @Column(name = "first_name")
    public String firstName;

    @NotNull
    @Column(name = "family_name_kana")
    public String familyNameKana;

    @NotNull
    @Column(name = "first_name_kana")
    public String firstNameKana;

    //M = 男性、F = 女性
    @NotNull
    @Column(name = "gender")
    public Gender gender;

    @Past
    @Column(name = "birthday")
    public LocalDate birthday;

    @NotNull
    @Column(name = "phone_number")
    public String phoneNumber;

    public static Finder<Long, Account> finder = new Finder<Long, Account>(Account.class);

    @Override
    public String toString() {
        return "Account[" + "id:" + id + " email_address:" + emailAddress + " password:" + password + " 確認状況:" + confirmation + " account_type:" +
                accountType + "]";
    }

    public String getInfo(){
        String info = "AccountId("+ id +") の情報[ 名前：" + getFullName() + "(" + familyNameKana + firstNameKana + ")";
        info += " 誕生日(年齢):" + formatBirthday() + "("+ getAge() +")" + " 性別:" + gender.getString() + " 電話番号:" + phoneNumber + "]";
        return info;
    }

    public Long getAge(){
        return ChronoUnit.YEARS.between(birthday, LocalDate.now());
    }

    //フルネームを返す
    public String getFullName(){
        return familyName + firstName;
    }

    //名前が空以外ならtrueを返す(送り仮名は空でもOK)
    public Boolean hasName(){
        return !getFullName().isEmpty();
    }

    //整形した誕生日を返す
    private String formatBirthday() {
        return birthday.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    }
}
