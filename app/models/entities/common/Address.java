package models.entities.common;

import models.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * Created by m-kato on 2017/05/26.
 * 住所を登録してあるEntity
 * User,Company,OfferがIdを持っている。
 * @see models.entities.User,models.entities.Company
 */
@Entity
@Table(name = "address")
public class Address extends BaseEntity{

    //初期化
    public Address(){
        this.postalCode = "";
        this.prefecture = "";
        this.city = "";
        this.houseNumber = "";
        this.building = "";
    }

    @NotNull
    @Column(name = "postal_code")
    public String postalCode;

    @NotNull
    @Column(name = "prefecture")
    public String prefecture;

    @NotNull
    @Column(name = "city")
    public String city;

    @NotNull
    @Column(name = "house_number")
    public String houseNumber;

    @Column(name = "building")
    public String building;

    public static Finder<Long, Address> finder = new Finder<Long, Address>(Address.class);

    @Override
    public String toString(){
        return "〒" + postalCode + " 都道府県:" + prefecture + " 市区町村:" + city + " 番地:" + houseNumber + " 建物名:" + building;
    }
}