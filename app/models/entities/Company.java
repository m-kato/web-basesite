package models.entities;

import models.entities.common.Address;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by m-kato on 2017/04/27.
 * 企業情報を登録するテーブル
 */
@Entity
@Table(name = "company")
public class Company extends BaseEntity {

    public Company(){
        this.address = new Address();
    }

    /**
     * Accountテーブルとのリレーション
     * Accountテーブルのidに格納されている
     */
    @NotNull
    @OneToMany(mappedBy = "company")
    public List<Agent> agentList;
    
    @NotNull
    @Column(name = "name")
    public String name;

    @NotNull
    @Column(name = "phone_number")
    public String phoneNumber;

    @OneToOne(cascade = CascadeType.ALL,targetEntity = Company.class)
    public Address address;

    public static Finder<Long, Company> finder = new Finder<Long, Company>(Company.class);

    @Override
    public String toString() {
        return "Company["+ id + " 企業名:" + name + " 住所:" + address + "]";
    }
}
