package models.entities;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.avaje.ebean.bean.EntityBean;
import play.data.format.Formats;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * Created by m-kato on 2017/04/26.
 * すべてのDBに自動的に挿入されるカラムを設定
 * Entityを実装する場合はBaseEntityを継承すること
 */
@MappedSuperclass
public abstract class BaseEntity extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    public Long id;

    @CreatedTimestamp
    @Formats.DateTime(pattern = "yyyy/MM/dd HH:mm:ss")
    @Column(name = "create_date")
    public LocalDateTime cteateDate;

    @UpdatedTimestamp
    @Formats.DateTime(pattern = "yyyy/MM/dd HH:mm:ss")
    @Column(name = "update_date")
    public LocalDateTime updateDate;

}
