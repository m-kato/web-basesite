package models.entities.mails;

import models.entities.Account;
import models.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by m-kato on 2017/05/16.
 * メールの送信者を記録するSenderEntity
 * 送信したメールのIDと送信したアカウントIDをもつ
 */
@Entity
@Table(name = "sender")
public class Sender extends BaseEntity{

    @NotNull
    @OneToOne
    @Column(name = "mail_id")
    public Mail mail;

    @NotNull
    @ManyToOne(targetEntity = Sender.class)
    public Account account;

}
