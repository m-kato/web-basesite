package models.entities.mails;

import models.entities.Account;
import models.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by m-kato on 2017/05/16.
 * メールの受信者を記録するReceiverEntity
 * 受信したメールのIDと受信したアカウントIDをもつ
 */
@Entity
@Table(name = "receiver")
public class Receiver extends BaseEntity{

    @NotNull
    @ManyToOne
    @Column(name = "mail_id")
    public Mail mail;

    @NotNull
    @ManyToOne(targetEntity = Receiver.class)
    @Column(name = "account_id")
    public Account account;

    @Column(name = "opened_flag")
    public Boolean opendFlag;

}
