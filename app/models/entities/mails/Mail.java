package models.entities.mails;

import models.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by m-kato on 2017/05/15.
 * ユーザのやり取りを保存するMailEntity
 * 送信者と受信者は別テーブルでもつ
 */
@Entity
@Table(name = "mail")
public class Mail extends BaseEntity {

    @NotNull
    @Column(name = "subject")
    public String subject;

    @NotNull
    @Column(name = "text_body")
    public String textBody;

    //送信者 FROM
    @Column(name = "sender")
    @OneToOne
    public Sender sender;

    //受信者 TO
    @Column(name = "receiver")
    @OneToMany
    public List<Receiver> receivers;

}
