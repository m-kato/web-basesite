package models.entities;

import com.avaje.ebean.Model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by m-kato on 2017/05/19.
 * ユーザーと企業のリレーション用Entity
 */
@Entity
@Table(name = "agent")
public class Agent extends BaseEntity{

    /**
     * 一意のAccountとCompanyを生成時に紐づける
     * 新規作成時のコンストラクタ
     */
    public Agent(Account account) {
        this.account = account;
        this.company = new Company();
    }

    /**
     * Agent追加時のコンストラクタ
     * @param account
     * @param company
     */
    public Agent(Account account,Company company) {
        this.account = account;
        this.company = company;
    }

    /**
     * Accountテーブルとのリレーション
     */
    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @Column(name = "account_id", unique = true)
    public Account account;

    /**
     * Companyとのリレーション
     */
    @NotNull
    @ManyToOne
    @Column(name = "company_id", unique = true)
    public Company company;

    public static Finder<Long, Agent> finder = new Finder<Long, Agent>(Agent.class);

    @Override
    public String toString(){
        return account.user.toString() + company;
    }
}
