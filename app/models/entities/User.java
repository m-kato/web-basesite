package models.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import static java.lang.Math.addExact;

/**
 * Created by m-kato on 2017/04/27.
 * 利用者の基本情報を保持するEntity
 */
@Entity
@Table(name = "user")
public class User extends BaseEntity {

    /**
     * 一意のアカウントIdをログイン情報取得し生成時に紐づける
     */
    public User(Account account) {
        this.account = account;
    }

    /**
     * Accountテーブルとのリレーション
     * Accountテーブルのidに格納されている
     */
    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @Column(name = "account_id")
    public Account account;


    public static Finder<Long, User> finder = new Finder<Long, User>(User.class);

    @Override
    public String toString() {
        return "User["+ account.getFullName() +"]";
    }
}