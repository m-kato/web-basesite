package models.services.user;

import com.google.inject.Inject;
import models.entities.Account;
import models.forms.account.AccountInfoForm;
import models.services.account.AccountService;

import java.time.LocalDate;
import java.util.Optional;

import static models.constants.AccountType.USER;
import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/04/27.
 * Userが行えるサービスのビジネスロジックを記述する。
 * DB操作を行う場合は、UserModelServiceを利用する。
 *
 * UserControllerから受け取った値を処理し、
 * UserControllerへ処理結果を返す。
 */
public class UserService extends AccountService {

    @Inject
    protected UserService userService;

    @Inject
    protected UserModelService userModelService;

    /**
     * Injectしないで使う場合はuse()でインスタンスを作ってサービスを使用できる
     *
     * @return Serviceのインスタンス
     */
    public static UserService use() {
        return new UserService();
    }

    public Boolean userRegister(AccountInfoForm entry){

        //ログイン中のアカウントを取得
        Account loginAccount = authenticator.getSessionAccount();
        //nullなら失敗
        if (loginAccount == null) return FAILURE;

        return changeAccountType(loginAccount,USER)? accountInfoRegister(entry,loginAccount): FAILURE;
    }
}
