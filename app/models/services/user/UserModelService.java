package models.services.user;

import models.entities.User;
import models.services.ModelService;

import java.util.Optional;

import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/04/28.
 * Userテーブル操作するモデルサービス
 * @see User
 */
public class UserModelService implements ModelService<User> {

    /**
     * Injectしないで使う場合はuse()でインスタンスを作ってサービスを使用できる
     *
     * @return ModelServiceのインスタンス
     */
    public static UserModelService use() {
        return new UserModelService();
    }

    //以下、検索処理

    /**
     * DB走査処理、PKを渡すと、一致したEntityを返す
     *
     * @param id
     * @return UserEntity
     */
    @Override
    public Optional<User> findById(Long id) {
        Optional<Long> userIdOps = Optional.of(id);
        if (userIdOps.isPresent()) {
            return Optional.ofNullable(User.finder.byId(id));
        }
        return Optional.empty();
    }

    //以下、登録、更新、削除処理

    /**
     * UserEntityを渡すとデータベースに登録する
     *
     * @param entry
     * @return 失敗ならNullを返す
     */
    @Override
    public Optional<User> save(User entry) {
        Optional<User> userOps = Optional.of(entry);
        if (userOps.isPresent()) {
            entry.save();
            return Optional.ofNullable(entry);
        }
        return Optional.empty();
    }

    /**
     * UserEntityを渡すとデータベースをその情報で更新する
     *
     * @param entry
     * @return 失敗ならNullを返す
     */
    @Override
    public Optional<User> update(User entry) {
        Optional<User> userOps = Optional.of(entry);
        if (userOps.isPresent()) {
            entry.update();
            return Optional.ofNullable(entry);
        }
        return Optional.empty();
    }

    /**
     * UserEntityを渡すとデータベースのそのEntityを削除する。
     *
     * @param entry
     * @return 成功ならtrue失敗ならfalseを返す
     */
    @Override
    public Boolean delete(User entry) {
        Optional<User> userOps = Optional.of(entry);
        if (userOps.isPresent()) {
            entry.delete();
            return SUCCESS;
        }
        return FAILURE;
    }
}
