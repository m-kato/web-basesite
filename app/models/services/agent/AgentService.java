package models.services.agent;

import com.google.inject.Inject;
import controllers.security.Authenticator;
import models.constants.AccountType;
import models.entities.Account;
import models.entities.Agent;
import models.entities.Company;
import models.entities.common.Address;
import models.forms.account.AccountInfoForm;
import models.forms.company.CompanyForm;
import models.services.account.AccountService;
import models.services.common.CommonService;
import models.services.user.UserService;

import java.util.Optional;

import static models.constants.AccountType.*;
import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/04/27.
 * Agentが行えるサービスのビジネスロジックを記述する。
 * DB操作を行う場合は、UserModelServiceを利用する。
 * <p>
 * AgentControllerから受け取った値を処理し、
 * AgentControllerへ処理結果を返す。
 */
public class AgentService extends AccountService {

    @Inject
    protected AgentService agentService;

    @Inject
    protected AgentModelService agentModelService;

    /**
     * Injectしないで使う場合はuse()でインスタンスを作ってサービスを使用できる
     *
     * @return Serviceのインスタンス
     */
    public static AgentService use() {
        return new AgentService();
    }

    /**
     * 企業側ユーザの詳細情報入力
     * アカウントタイプの変更と、詳細情報の更新
     * 企業ユーザと会社情報の紐づけがすべて成功しないとfalseを返す。
     * @param entry
     * @return 結果
     */
    public Boolean agentUserRegister(AccountInfoForm entry){

        Account loginAccount = authenticator.getSessionAccount();

        //アカウントのタイプ変更
        if(changeAccountType(loginAccount,AGENT)){

            //詳細情報の登録
            if(accountInfoRegister(entry,loginAccount)){

                //AccountとCompanyのリレーション作成
                //すべて成功しないとtrueを返さない
                return addAgent(loginAccount);
            }
        }
        return FAILURE;
    }

    /**
     * account渡すと新規企業を作成しリレーションを登録する。
     * @param account
     * @return 登録結果
     */
    public Boolean addAgent(Account account) {

        //会社とユーザーのリレーション作成
        Agent agent = new Agent(account);

        //セーブが成功したらSUCCESSを返す
        Optional<Agent> savedAgent = agentModelService.save(agent);

        return savedAgent.isPresent() ? SUCCESS : FAILURE;
    }

    /**
     * Created by m-kato on 2017/04/27.
     * Agentが行えるCompanyサービスのビジネスロジックを記述するインナークラス。
     * DB操作を行う場合は、CompanyModelServiceを利用する。
     */
    class CompanyService{

        @Inject
        CommonService commonService;

        @Inject
        CompanyModelService companyModelService;

        /**
         * Injectしないで使う場合はuse()でインスタンスを作ってサービスを使用できる
         * @return Serviceのインスタンス
         */
        public CompanyService use() {
            return new CompanyService();
        }

        /**
         * 企業の登録メソッド。
         * @param entry
         * @return 登録結果
         */
        public Boolean companyRegister(CompanyForm entry) {

            //ログイン中のアカウントを取得
            Account loginAccount = authenticator.getSessionAccount();

            /**
             * Agentが登録できていれば企業登録もコンストラクタでされているはず
             * @see models.entities.Agent
             */
            Optional<Agent> agentOps = agentModelService.findByAccount(loginAccount);
            Company company = agentOps.orElse(null).company;

            //companyがなければ(検索失敗かcompanyのリレーションが張られていない)失敗
            if (company != null) {
                company.name = entry.companyName;
                company.phoneNumber = entry.phoneNumber;

                //企業の住所に情報を入れる
                company.address.postalCode = entry.postalCode;
                company.address.prefecture = entry.prefecture;
                company.address.city = entry.city;
                company.address.houseNumber = entry.houseNumber;
                company.address.building = entry.building;
            } else {
                return FAILURE;
            }

            //セーブが成功したらSUCCESSを返す
            Optional<Company> savedCompany = companyModelService.update(company);
            return savedCompany.isPresent() ? SUCCESS : FAILURE;
        }

    }
}
