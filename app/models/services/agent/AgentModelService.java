package models.services.agent;

import models.entities.Account;
import models.entities.Agent;
import models.entities.User;
import models.services.ModelService;

import java.util.Optional;

import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/04/28.
 * Agentテーブル操作するモデルサービス
 *
 * @see User
 */
public class AgentModelService implements ModelService<Agent> {

    /**
     * Injectしないで使う場合はuse()でインスタンスを作ってサービスを使用できる
     *
     * @return ModelServiceのインスタンス
     */
    public static AgentModelService use() {
        return new AgentModelService();
    }

    //以下、検索処理

    /**
     * DB走査処理、PKを渡すと、一致したEntityを返す
     *
     * @param id
     * @return AgentEntity
     */
    @Override
    public Optional<Agent> findById(Long id) {
        Optional<Long> agentIdOps = Optional.of(id);
        if (agentIdOps.isPresent()) {
            return Optional.ofNullable(Agent.finder.byId(id));
        }
        return Optional.empty();
    }

    public Optional<Agent> findByAccount(Account account) {
        Optional<Account> accountIdOps = Optional.ofNullable(account);
        if (accountIdOps.isPresent()) {
            return Optional.ofNullable(Agent.finder.where().eq("account",account).findUnique());
        }
        return Optional.empty();
    }

    //以下、登録、更新、削除処理

    /**
     * AgentEntityを渡すとデータベースに登録する
     *
     * @param entry
     * @return 失敗ならNullを返す
     */
    @Override
    public Optional<Agent> save(Agent entry) {
        Optional<Agent> agentOps = Optional.of(entry);
        if (agentOps.isPresent()) {
            entry.save();
            return Optional.ofNullable(entry);
        }
        return Optional.empty();
    }

    /**
     * AgentEntityを渡すとデータベースをその情報で更新する
     *
     * @param entry
     * @return 失敗ならNullを返す
     */
    @Override
    public Optional<Agent> update(Agent entry) {
        Optional<Agent> agentOps = Optional.of(entry);
        if (agentOps.isPresent()) {
            entry.update();
            return Optional.ofNullable(entry);
        }
        return Optional.empty();
    }

    /**
     * AgentEntityを渡すとデータベースのそのEntityを削除する。
     *
     * @param entry
     * @return 成功ならtrue失敗ならfalseを返す
     */
    @Override
    public Boolean delete(Agent entry) {
        Optional<Agent> agentOps = Optional.of(entry);
        if (agentOps.isPresent()) {
            entry.delete();
            return SUCCESS;
        }
        return FAILURE;
    }
}
