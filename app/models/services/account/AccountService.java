package models.services.account;

import com.google.inject.Inject;
import controllers.security.Authenticator;
import models.constants.AccountType;
import models.entities.Account;
import models.forms.account.AccountForm;
import models.forms.account.AccountInfoForm;
import models.forms.account.LoginForm;
import models.forms.checks.Check;
import models.services.common.CommonService;

import java.time.LocalDate;
import java.util.Optional;

import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/04/28.
 * アカウントについてのサービス処理を記述
 * ここではDB処理は行わず、ビジネスロジックのみ記述する。
 */
public class AccountService{

    @Inject
    protected Authenticator authenticator;

    @Inject
    protected AccountService accountService;

    @Inject
    protected AccountModelService accountModelService;

    /**
     * Injectしないで使う場合はuse()でインスタンスを作ってサービスを使用できる
     *
     * @return Serviceのインスタンス
     */
    public static AccountService use() {
        return new AccountService();
    }

    /**
     * Accountの登録処理
     * バリデーションはコントローラで行う
     *
     * @param entry
     * @return 成功or失敗
     */
    public Boolean accountRegister(AccountForm entry) {

        //フォームの取得失敗
        if (entry == null) return FAILURE;
        //既に同じメールが登録されている場合も失敗
        if(accountModelService.findByEmailAddress(entry.emailAddress).isPresent())return FAILURE;

        //登録されていなければ新規作成
        Account account = new Account();
        account.emailAddress = entry.emailAddress;
        account.password = entry.password;

        //新規登録して作成
        Optional savedAccount = accountModelService.save(account);
        return savedAccount.isPresent() ? SUCCESS : FAILURE;
    }

    /**
     * 基本情報の登録メソッド
     *
     * @param entry
     * @param account
     * @return
     */
    public Boolean accountInfoRegister(AccountInfoForm entry, Account account) {

        //フォームの取得失敗かaccount取得失敗
        if (entry == null || account == null) return FAILURE;

        //formのデータを格納
        account.familyName = entry.familyName;
        account.firstName = entry.firstName;
        account.familyNameKana = entry.familyNameKana;
        account.firstNameKana = entry.firstNameKana;
        account.gender = entry.gender;
        account.birthday = LocalDate.parse(entry.birthday);
        account.phoneNumber = entry.phoneNumber;

        //成功なら登録したら結果を返す。
        return update(account);
    }


    /**
     * Accountの更新処理
     *
     * @param account
     * @return Boolean
     */
    public Boolean update(Account account) {

        //不正なAccountなら失敗
        if (account == null) return FAILURE;

        //アップデート結果を返す
        Optional<Account> savedAccount = accountModelService.update(account);
        return savedAccount.isPresent() ? SUCCESS : FAILURE;
    }

    /**
     * 認証メソッド
     *
     * @param entry
     * @return 成功はtrue、失敗はfalseを返す
     */
    public Boolean authenticate(LoginForm entry) {

        //入力したメールアドレスで検索して同じものがあればAccountを取得する
        Optional<Account> accountOps = accountModelService.findByEmailAddress(entry.emailAddress);
        Account account = accountOps.orElse(null);

        //Accountが見つかからなければ失敗
        if (account == null) {
            return FAILURE;
        }
        return Check.use().EqualCheck(account.password, entry.password);
    }

    /**
     * アカウントタイプ変更メソッド
     *
     * @param account,accountType
     * @return 結果
     */
    public Boolean changeAccountType(Account account, AccountType accountType) {

        if (Optional.ofNullable(account).isPresent()) {
            account.accountType = accountType;
        }else {
            return FAILURE;
        }
        return update(account);
    }

    /**
     * 比較対象のAccountTypeを検証する
     *
     * @param accountType
     * @return 結果
     */
    public void logoutIllegalAccount(AccountType accountType) {
        try {
            //ログイン中のアカウントを取得
            Account loginAccount = authenticator.getSessionAccount();
            if (!loginAccount.accountType.equals(accountType)) {
                authenticator.sessionClear();
            }
        } catch (NullPointerException e) {
            authenticator.sessionClear();
            System.out.println(e);
        }
    }
}
