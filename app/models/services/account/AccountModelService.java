package models.services.account;

import models.entities.Account;
import models.services.ModelService;

import java.util.Optional;

import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/04/27.
 * アカウントテーブルを操作するモデルサービス
 *
 * @see Account
 */
public class AccountModelService implements ModelService<Account> {

    /**
     * Injectしないで使う場合はuse()でインスタンスを作ってサービスを使用できる
     *
     * @return AccountModelServiceのインスタンス
     */
    public static AccountModelService use() {
        return new AccountModelService();
    }

    //以下、検索処理

    /**
     * DB走査処理、PKを渡すと、一致したEntityを返す
     *
     * @param id
     * @return AccountEntity or Optional.empty
     */
    @Override
    public Optional<Account> findById(Long id) {
        Optional<Long> accountIdOps = Optional.ofNullable(id);
        if (accountIdOps.isPresent()) {
            return Optional.ofNullable(Account.finder.byId(id));
        }
        return Optional.empty();
    }

    /**
     * DB走査処理、emailを渡すと、一致したEntityを返す
     *
     * @param email
     * @return AccountEntity
     */
    public Optional<Account> findByEmailAddress(String email) {
        Optional<String> emailOps = Optional.ofNullable(email);
        if (emailOps.isPresent()) {
            return Optional.ofNullable(Account.finder.where().eq("emailAddress", email).findUnique());
        }
        return Optional.empty();
    }

    //以下、登録・更新・削除

    /**
     * AccountEntityを渡すとデータベースに登録する
     *
     * @param entry
     * @return AccountEntity or Optional.empty
     */
    @Override
    public Optional<Account> save(Account entry) {
        Optional<Account> accountOps = Optional.ofNullable(entry);
        if (accountOps.isPresent()) {
            entry.save();
            return Optional.ofNullable(entry);
        }
        return Optional.empty();
    }

    /**
     * AccountEntityを渡すとデータベースをその情報で更新する
     *
     * @param entry
     * @return AccountEntity or Optional.empty
     */
    @Override
    public Optional<Account> update(Account entry) {
        Optional<Account> accountOps = Optional.ofNullable(entry);
        if (accountOps.isPresent()) {
            entry.update();
            return Optional.ofNullable(entry);
        }
        return Optional.empty();
    }

    /**
     * AccountEntityを渡すとデータベースのそのEntityを削除する。
     *
     * @param entry
     * @return 成功ならtrue失敗ならfalseを返す
     */
    @Override
    public Boolean delete(Account entry) {
        Optional<Account> accountOps = Optional.of(entry);
        if (accountOps.isPresent()) {
            entry.delete();
            return SUCCESS;
        }
        return FAILURE;
    }
}
