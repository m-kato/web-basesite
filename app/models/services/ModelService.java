package models.services;

import com.avaje.ebean.Model;

import java.util.Optional;

/**
 * Created by m-kato on 2017/04/27.
 * DB操作用モデルサービスのインターフェイス
 * DB操作するサービスクラスはこのクラスをインプリメントすること
 */
public interface ModelService<Entity extends Model> {

    public Optional<Entity> findById(Long Id);

    public Optional<Entity> save(Entity entry);

    public Optional<Entity> update(Entity entry);

    public Boolean delete(Entity entry);
}
