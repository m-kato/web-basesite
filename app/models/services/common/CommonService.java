package models.services.common;

import com.google.inject.Inject;
import models.entities.common.Address;

import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/05/29.
 */
public class CommonService {

    @Inject
    AddressModelService addressModelService;

    public Boolean addressRegister(Address address){
        return addressModelService.save(address).isPresent() ? SUCCESS:FAILURE;
    }
}
