package models.services.common;

import models.entities.common.Address;
import models.services.ModelService;

import java.util.Optional;

import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/04/28.
 * Agentテーブル操作するモデルサービス
 * @see models.entities.common.Address
 */
public class AddressModelService implements ModelService<Address> {

    /**
     * Injectしないで使う場合はuse()でインスタンスを作ってサービスを使用できる
     *
     * @return ModelServiceのインスタンス
     */
    public static AddressModelService use() {
        return new AddressModelService();
    }

    //以下、検索処理

    /**
     * DB走査処理、PKを渡すと、一致したEntityを返す
     * @param id
     * @return AgentEntity
     */
    @Override
    public Optional<Address> findById(Long id) {
        Optional<Long> targetIdOps = Optional.ofNullable(id);
        if (targetIdOps.isPresent()) {
            return Optional.ofNullable(Address.finder.byId(id));
        }
        return Optional.empty();
    }

    //以下、登録、更新、削除処理

    /**
     * Entityを渡すとデータベースに登録する
     *
     * @param entry
     * @return 失敗ならNullを返す
     */
    @Override
    public Optional<Address> save(Address entry) {
        Optional<Address> targetOps = Optional.ofNullable(entry);
        if (targetOps.isPresent()) {
            entry.save();
            return Optional.ofNullable(entry);
        }
        return Optional.empty();
    }

    /**
     * AgentEntityを渡すとデータベースをその情報で更新する
     *
     * @param entry
     * @return 失敗ならNullを返す
     */
    @Override
    public Optional<Address> update(Address entry) {
        Optional<Address> targetOps = Optional.ofNullable(entry);
        if (targetOps.isPresent()) {
            entry.update();
            return Optional.ofNullable(entry);
        }
        return Optional.empty();
    }

    /**
     * AgentEntityを渡すとデータベースのそのEntityを削除する。
     *
     * @param entry
     * @return 成功ならtrue失敗ならfalseを返す
     */
    @Override
    public Boolean delete(Address entry) {
        Optional<Address> targetOps = Optional.of(entry);
        if (targetOps.isPresent()) {
            entry.delete();
            return SUCCESS;
        }
        return FAILURE;
    }
}
