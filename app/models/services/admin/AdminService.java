package models.services.admin;

import com.google.inject.Inject;
import models.entities.Account;
import models.entities.Company;
import models.entities.User;
import models.services.account.AccountService;
import models.services.user.UserService;

import java.util.List;

/**
 * Created by m-kato on 2017/04/28.
 */
public class AdminService extends AccountService {

    @Inject
    AdminModelService adminModelService;

    @Inject
    UserService userService;

    public List<Account> getAccountList() {
        return adminModelService.findByAllAccount();
    }

    public List<User> getUserList() {
        return adminModelService.findByAllUser();
    }

    public List<Company> getCompanyList() {
        return adminModelService.findByAllCompany();
    }
}
