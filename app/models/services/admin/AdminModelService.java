package models.services.admin;

import com.google.inject.Inject;
import models.entities.Account;
import models.entities.Company;
import models.entities.User;
import models.services.account.AccountModelService;
import models.services.user.UserModelService;

import java.util.List;

/**
 * Created by m-kato on 2017/04/28.
 */
public class AdminModelService {

    @Inject
    AccountModelService accountModelService;

    @Inject
    UserModelService userModelService;

    public List<Account> findByAllAccount() {
        return Account.finder.all();
    }

    public List<User> findByAllUser() {
        return User.finder.all();
    }

    public List<Company> findByAllCompany() {
        return Company.finder.all();
    }

}
