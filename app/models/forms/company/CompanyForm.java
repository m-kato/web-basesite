package models.forms.company;

import models.forms.checks.AccountFormCheck;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by m-kato on 2017/04/27.
 * 企業情報の登録・更新・変更時のFormクラス
 */
public class CompanyForm {

    /**
     * 企業情報入力フォーム
     */
    @Constraints.Required(message = "企業名を入力してください")
    public String companyName;

    @Constraints.Required(message = "電話番号を入力してください")
    @Constraints.Pattern("[0-9]+")
    public String phoneNumber;

    @Constraints.Required
    public String postalCode;

    @Constraints.Required
    public String prefecture;

    @Constraints.Required
    public String city;

    @Constraints.Required
    public String houseNumber;

    public String building;


    /**
     * 入力データの妥当性チェック
     * @return 空のリストならエラーがないのでOK、Nullを返す。エラー情報
     */
    public List<ValidationError> validate(){
        List<ValidationError> errors = new ArrayList<>();
        return errors.isEmpty() ? null : errors;
    }
}
