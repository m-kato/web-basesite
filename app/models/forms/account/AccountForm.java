package models.forms.account;

import models.forms.checks.AccountFormCheck;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by m-kato on 2017/04/27.
 * アカウント情報の登録・更新・変更時のFormクラス
 */
public class AccountForm{

    /**
     * ログイン用メールアドレス入力フォーム
     */
    @Constraints.Required(message = "メールアドレスが入力されていません")
    @Constraints.Email(message = "正しい形式で入力してください")
    public String emailAddress;

    /**
     * ログイン用パスワード入力フォームと確認用フォーム
     */
    @Constraints.Required(message = "パスワードが入力されていません")
    @Constraints.Pattern("[a-zA-Z0-9]+")
    public String password;

    @Constraints.Pattern("[a-zA-Z0-9]+")
    public String passwordCheck;

    /**
     * 入力データの妥当性チェック
     * @return 空のリストならエラーがないのでOK、Nullを返す。エラー情報
     */
    public List<ValidationError> validate(){
        List<ValidationError> errors = new ArrayList<>();
        AccountFormCheck check = new AccountFormCheck();
        errors = check.execute(this, errors);
        return errors.isEmpty() ? null : errors;
    }
}
