package models.forms.account;

import models.constants.Gender;
import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by m-kato on 2017/04/27.
 * ユーザ情報の登録・更新・変更時のFormクラス
 */
public class AccountInfoForm {

    /**
     * 基本情報入力フォーム
     */
    //苗字
    @Constraints.Required(message = "必須")
    public String familyName;

    //名前
    @Constraints.Required(message = "必須")
    public String firstName;

    //苗字読み
    @Constraints.Required(message = "必須")
    public String familyNameKana;

    //名前読み
    @Constraints.Required(message = "必須")
    public String firstNameKana;

    //性別 男性=MALE 女性=FEMALE
    @Constraints.Required(message = "必須")
    public Gender gender;

    //誕生日
    @Constraints.Required(message = "必須：生年月日を入力してください")
    @Constraints.MaxLength(value = 10,message = "yyyy/MM/ddの形式で入力してください 例)1990/01/01")
    @Formats.DateTime(pattern = "yyyy/MM/dd")
    public String birthday;

    //電話番号
    @Constraints.Required(message = "必須：電話番号を入力してください")
    @Constraints.Pattern("[0-9]+")
    public String phoneNumber;
}
