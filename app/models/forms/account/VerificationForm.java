package models.forms.account;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by m-kato on 2017/04/27.
 * 初回登録時やメール変更時の2段階認証用VerificationFormクラス
 */
public class VerificationForm {

    /**
     * 二段階認証用トークン確認フォーム
     */
    @Constraints.Required(message = "6ケタの半角数字で入力してください")
    @Constraints.Pattern(value = "[0-9]+",message = "6ケタの半角数字で入力してください")
    public String token;


    /**
     * 入力データの妥当性チェック
     * @return 空のリストならエラーがないのでOK、Nullを返す。エラー情報
     */
    public List<ValidationError> validate(){
        List<ValidationError> errors = new ArrayList<>();
        return errors.isEmpty() ? null : errors;
    }
}
