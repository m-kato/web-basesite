package models.forms.checks;

/**
 * Created by m-kato on 2017/05/01.
 * Formのバリデーションチェッククラス
 * 入力内容をチェックする
 */
public class Check {

    public static Check use(){return new Check();}

    //文字列を比較して同じならtrueを返す。
    public Boolean EqualCheck(String strA, String strB){
        return strA.equals(strB);
    }
}
