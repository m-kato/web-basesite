package models.forms.checks;

import models.forms.account.AccountForm;
import play.data.validation.ValidationError;

import java.util.List;

/**
 * Created by m-kato on 2017/05/01.
 *
 */
public class AccountFormCheck extends Check {

    public static AccountFormCheck use(){return new AccountFormCheck();}

    public List<ValidationError> execute(AccountForm accountForm, List<ValidationError> errors){
        if(!EqualCheck(accountForm.password,accountForm.passwordCheck)){
            errors.add(new ValidationError("password","再入力パスワードと一致しません"));
            errors.add(new ValidationError("passwordCheck","パスワードと一致しません"));
        }
        return errors;
    }
}
