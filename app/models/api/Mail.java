package models.api;

import models.entities.Account;
import play.libs.mailer.MailerClient;
import play.libs.mailer.Email;

import static models.constants.Constants.*;
import static models.constants.Util.*;

/**
 * Created by m-kato on 2017/05/15.
 * ユーザ間のやり取りを管理するメールクラス。
 */
public class Mail{

    private Email email;

    //メールコンストラクタ
    //new Mail()で同時にEmailを作成する
    public Mail(){
        this.email = new Email();
    }

    //メール送信 成功=true 失敗=false

    /**
     * ユーザ間のメール連絡処理
     * DBに内容を登録して、サイト内でメール履歴を閲覧できるように
     * @param mailerClient
     * @param email
     * @return 送信結果
     */
    private boolean sendEmail(MailerClient mailerClient, Email email) {
        boolean result = FAILURE;
        try {
            mailerClient.send(email);
            //TODO メールのデータベース登録処理を記述
            result = SUCCESS;
        } catch (Exception e) {
            play.Logger.error(e.toString());
        }
        return result;
    }

    /**
     * システムからのメール送信、DBに送信内容を登録しない
     * @return メール送信 成功=true 失敗=false
     */
    private boolean sendSystemEmail(MailerClient mailerClient, Email email) {
        //システムの送信専用メールアドレスをセット
        email.setFrom(SYSTEM_EMAIL_ADDRESS);

        //送信処理、DB登録なし
        boolean result = FAILURE;
        try {
            mailerClient.send(email);
            result = SUCCESS;
        } catch (Exception e) {
            play.Logger.error(e.toString());
        }
        return result;
    }

    //クライアントとアカウント情報を渡すと,
    //メールが利用可能かどうかのメールを送信
    public boolean emailCheck(MailerClient mailerClient,Account account,String token){
        email.addTo(account.emailAddress);
        email.setSubject("【" + ABBREVIATION_NAME + "】登録ご確認メール");
        //2段階認証メールの本文を作成
        String strText = views.html.mail.system.twoStepVerification.render(account,token).toString();
        email.setBodyText(strText);
        return sendSystemEmail(mailerClient,email);
    }
}
