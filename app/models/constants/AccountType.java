package models.constants;

/**
 * Created by m-kato on 2017/05/17.
 */
public enum AccountType {

    PROVISIONAL("仮登録"),
    USER("求職者"),
    AGENT("採用担当"),
    COMPANY("法人"),
    ADMIN("管理者");

    private final String stringValue;

    private AccountType(final String stringValue) {
        this.stringValue = stringValue;
    }

    public String getString() {
        return this.stringValue;
    }

}
