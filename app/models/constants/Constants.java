package models.constants;

/**
 * Created by m-kato on 2017/05/02.
 */
public class Constants {

    // システムのコンテキスト名：サイトタイトル
    public static final String CONTEXT_NAME = "BEST JOB ～TEST～";

    // システムのコンテキスト名：略称
    public static final String ABBREVIATION_NAME = "BEST JOB TEST";

    // サイトルート名
    public static final String ROOT_NAME = "test";

    // システムのコンテキスト名：求人サイトのジャンル
    public static final String SITE_TYPE = "ポータル";

    public static final String SYSTEM_EMAIL_ADDRESS = "BEST-JOB-TEST@bestjob.co.jp";

    public static final String HOME_DIR = "TEST";


}
