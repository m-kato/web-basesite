package models.constants;

/**
 * Created by m-kato on 2017/04/28.
 * 汎用定数クラス
 */
public class Util {

    /**
     * 処理の成功/失敗をBoolean形で返す用
     */
    public static final Boolean SUCCESS = true;
    public static final Boolean FAILURE = false;

}
