package models.constants;


import com.avaje.ebean.annotation.EnumValue;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by m-kato on 2017/05/17.
 */
public enum  Gender {

    MALE("男性"),
    FEMALE("女性");

    private final String japanese;

    private Gender(final String japanese) {
        this.japanese = japanese;
    }

    public String getString() {
        return this.japanese;
    }

    /**
     * すべての要素
     * @return 列挙型のすべての識別子をMap<String,String>で返す。
     * @b4用 optionsに利用するSeq用
     */
    public static Map<String, String> valuesAsMap() {
        Map<String, String> map = new LinkedHashMap<>();
        //ViewでのtoSeqでScalaSeqにしている。
        //DB上は列挙子で保存される
        for (Gender gender : Gender.values()) {
            map.put(gender.name(),gender.getString());
        }
        //unmodifiableMap(map)でmapの変更不可能なビューを定数に代入。
        return Collections.unmodifiableMap(map);
    }

}
