package models.services.account;

import apps.FakeApp;
import models.constants.AccountType;
import models.constants.Gender;
import models.entities.Account;
import models.forms.account.AccountForm;
import models.forms.account.AccountInfoForm;
import models.forms.account.LoginForm;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static models.constants.Util.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
/**
 * Created by m-kato on 2017/05/18.
 * AccountServiceの単体テストクラス
 */
public class AccountServiceTest extends FakeApp {

    @Test
    public void use() throws Exception {
        AccountService accountService = AccountService.use();
        assertThat(accountService, is(instanceOf(AccountService.class)));
        assertNotNull(accountService);
    }

    /**
     * @throws Exception
     */
    @Test
    public void accountRegister() throws Exception {
        AccountForm accountForm = new AccountForm();
        accountForm.emailAddress = "test@test";
        accountForm.password = "test";

        Boolean result = accountService.accountRegister(null);//エントリーの取得ができなかったら失敗
        assertFalse(result);
        result = accountService.accountRegister(accountForm); //初回登録
        assertTrue(result);
        result = accountService.accountRegister(accountForm); //同じメールは登録できない
        assertFalse(result);
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void accountInfoRegister() throws Exception {
        Account account = new Account();
        account.emailAddress = "test@test";
        account.password = "test";
        accountModelService.save(account);

        AccountInfoForm accountInfoForm = new AccountInfoForm();
        accountInfoForm.familyName = "苗字";
        accountInfoForm.firstName = "名前";
        accountInfoForm.familyNameKana = "ミョウジ";
        accountInfoForm.firstNameKana = "ナマエ";
        accountInfoForm.gender = Gender.MALE;
        accountInfoForm.birthday = "1991-01-01";
        accountInfoForm.phoneNumber = "09012341234";

        Boolean result = accountService.accountInfoRegister(null, account);//フォームの取得失敗
        assertFalse(result);
        result = accountService.accountInfoRegister(accountInfoForm, null);//アカウントの取得失敗
        assertFalse(result);
        result = accountService.accountInfoRegister(accountInfoForm, account);//アカウント情報更新
        assertTrue(result);
    }


    @Test
    public void update() throws Exception {
        //新規アカウント登録
        Account account = new Account();
        account.emailAddress = "test@test";
        account.password = "test";
        account = accountModelService.save(account).orElse(null);

        //初回登録時のメールとパスワードが正しく登録されていること
        assertNotNull(account);
        assertEquals("test@test",account.emailAddress);
        assertEquals("test",account.password);

        //アカウントが検索できる
        Account savedAccount = accountModelService.findByEmailAddress("test@test").orElse(null);
        assertEquals(account,savedAccount);

        //情報をアップデートで更新できる
        savedAccount.emailAddress = "test@test2";
        savedAccount.password = "test2";
        Boolean result = accountService.update(savedAccount);
        assertTrue("アップデート失敗",result);//セーブ結果がBooleanで返ってくる

        //更新したアカウントがちゃんと検索できて更新されている
        Account changedAccount = accountModelService.findByEmailAddress("test@test2").orElse(null);
        assertNotNull("検索失敗",changedAccount);//nullではない
        assertEquals(account,changedAccount);//メール変更後も同じユーザを検索できている
        assertEquals("test@test2",changedAccount.emailAddress);//変更できている
        assertEquals("test2",changedAccount.password);//変更できている
    }


    @Test
    public void authenticate() throws Exception {
        //新規アカウント登録
        Account account = new Account();
        account.emailAddress = "test@test";
        account.password = "test";
        accountModelService.save(account);

        LoginForm loginForm = new LoginForm();

        //メールが違う
        loginForm.emailAddress = "ErrorTest@test";
        loginForm.password = "test";
        assertFalse(accountService.authenticate(loginForm));

        //パスワードが違う
        loginForm.emailAddress = "test@test";
        loginForm.password = "ErrorTest";
        assertFalse(accountService.authenticate(loginForm));

        //認証OK
        loginForm.emailAddress = "test@test";
        loginForm.password = "test";
        assertTrue(accountService.authenticate(loginForm));
    }

    @Test
    public void changeAccountType() throws Exception {
        //新規アカウント登録
        Account account = new Account();
        account.emailAddress = "test@test";
        account.password = "test";
        account = accountModelService.save(account).orElse(null);

        assertNotNull("セーブ失敗",account);
        assertEquals(AccountType.PROVISIONAL,account.accountType);
    }

}