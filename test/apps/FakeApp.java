package apps;

import com.avaje.ebean.Ebean;
import models.entities.Account;
import models.services.account.AccountModelService;
import models.services.account.AccountService;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import play.Application;
import play.test.WithApplication;

import static org.mockito.Mockito.*;
import static play.test.Helpers.*;

/**
 * DESCRIPTION
 *
 * @author harakazuhir
 * @since 2013/08/26 0:19
 */
public class FakeApp extends WithApplication {

    public static String createDdl = "";
    public static String dropDdl = "";

    //WithApplicationをオーバーライド
    @Override
    protected Application provideApplication() {
        return fakeApplication(inMemoryDatabase());
    }

    @Before
    public void createCleanDb() throws Exception {
        String evolutionContent = FileUtils.readFileToString(app.getWrappedApplication().getFile("conf/evolutions/default/1.sql"));
        String[] splitEvolutionContent = evolutionContent.split("# --- !Ups");
        String[] upsDowns = splitEvolutionContent[1].split("# --- !Downs");
        createDdl = upsDowns[0];
        dropDdl = upsDowns[1];
        initDb();
        setUp();
    }

    public static void initDb() {
        Ebean.execute(Ebean.createCallableSql(dropDdl));
        Ebean.execute(Ebean.createCallableSql(createDdl));

        // Ehcacheキャッシュのクリア
        CacheManager manager = CacheManager.create();
        Cache cache = manager.getCache("play");
        cache.removeAll();
    }

    public AccountService accountService;
    public AccountModelService accountModelService;

    public void setUp() {
        accountService = app.injector().instanceOf(AccountService.class);
        accountModelService = app.injector().instanceOf(AccountModelService.class);
    }

    public void restartApp() {
        start(app);
    }

    @After
    public void stopApp() {
        stop(app);
    }

}
