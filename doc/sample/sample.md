マークダウンで詳細仕様書を記述するテスト
====================

概要
----
マークダウン記法で詳細仕様書をプロジェクトで管理し、  
gitでバージョン管理を行うための試作を行う

文字の書き方
------

**【見出しの書き方】**  
setext形式の見出し記法を採用、文字の下に = か - 記載

H1
============
H2
------------


文字の強調
-----
*イタリック*  
**ボールドになる**  
***イタリックかつボールド***  

改行
---
改行は文章の末尾に半角2つ。

Settings > Editor > Other > Strip trailing spaces on Save を None に設定して  
AutoSave時の末尾の空白削除を解除しておく。


リンクの挿入
------
**自動リンクの書き方**  
<http://example.com>  
<example@example.com>  

```
<http://example.com>
<example@example.com>
```

**通常の書き方**  
[Google](http://google.com/ "グーグルだよ")
```
[リンクテキスト](アドレス "タイトル")
```

**外部参照リンクの書き方**

参照先をまとめて書くこともできる

[Yahoo検索][yahoo] or [MSN検索][msn]

[yahoo]:http://search.yahoo.com/  "Yahoo Search"
[msn]:http://search.msn.com/    "MSN Search"

```
[リンクタイトルA][linkref A] or [リンクタイトルB][linkref B]

[linkref A]:http://address/A/  "サイトA"
[linkref B]:http://address/B/  "サイトB"
```


**画像の挿入**

![Alt text](./image.png)
```
![代替テキスト](/path/to/img.jpg "イメージタイトル")
```

・リンクと同じように参照表記も可能

![Alt text][id]

[id]: ./image.png  "イメージタイトル"
```
![代替テキスト][id]

[id]: /path/to/img.jpg  "イメージタイトル"
```

コードの挿入
------
```
hoge = fuga;
```

    aaa
    aaa
    aaa
    aaa

`test`

連番の作成
-----

空白3つでネストできる

* リスト1
   * リストリスト1-1
   * リストリスト1-2
      * リストリスト1-2-3
* リスト2

1. 順番つきリスト
   1. 順番つきリスト
   1. 順番つきリスト
      1. 順番つきリスト
1. 順番つきリスト

>引用しました  
>引用しました  
>引用しました
>>引用しました
>引用しました

-----

***

特殊文字の表示
---
文字中の`&`は`&amp;`などに変換することで表示できる。

**エスケープ文字一覧**
```
&   ampersand
\   backslash
`   backtick
*   asterisk
_   underscore
{}  curly braces
[]  square brackets
()  parentheses
#   hash mark
+   plus sign
-   minus sign (hyphen)
.   dot
!   exclamation mark
```