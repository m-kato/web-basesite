//Jacocoの設定ファイル
import de.johoop.jacoco4sbt._

import JacocoPlugin._

jacoco.settings

//Playで並列実行はできないので切る
parallelExecution      in jacoco.Config := false

//出力先変更
jacoco.outputDirectory in jacoco.Config := file("public/jacoco")

//リポート形式設定
jacoco.reportFormats   in jacoco.Config := Seq(HTMLReport("utf-8"))

//Coverageの例外設定
jacoco.excludes        in jacoco.Config := Seq("views*", "*Routes*", "controllers*routes*", "controllers*Reverse*", "controllers*javascript*", "controller*ref*")