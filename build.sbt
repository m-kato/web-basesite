name := """base"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.7"

//依存性の解消
resolvers += Resolver.mavenLocal
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

//コンパイラ指定
javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-encoding", "UTF-8")

//ライブラリの追加
libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  filters,
  "org.mindrot" % "jbcrypt" % "0.3m",
  "com.adrianhurt" %% "play-bootstrap" % "1.1-P25-B4",
  "org.webjars" % "bootstrap" % "4.0.0-alpha.6",
  "org.webjars" %% "webjars-play" % "2.4.0-2",
  "org.mariadb.jdbc" % "mariadb-java-client" % "1.4.4",
  "com.typesafe.play" %% "play-mailer" % "5.0.0-M1",
  "junit" % "junit" % "4.11" % Test,
  "com.novocode" % "junit-interface" % "0.11" % Test,
  "org.mockito" % "mockito-all" % "1.10.19" % Test
)

routesGenerator := InjectedRoutesGenerator

//ビルド時に定数を読み込んで、importなしで定数を使えるように
TwirlKeys.templateImports ++= Seq(
  "models.constants._",
  "models.constants.Util._",
  "models.constants.Constants._"
  //	"models.constants.Jobtype._",
  //  "models.constants.Competence._",
  //	"models.constants.Message._",
  //  "models.constants.FormConstants._"
)